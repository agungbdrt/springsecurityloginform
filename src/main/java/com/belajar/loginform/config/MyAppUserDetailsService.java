package com.belajar.loginform.config;

import com.belajar.loginform.Repository.UserInfoRepository;
import com.belajar.loginform.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class MyAppUserDetailsService implements UserDetailsService {

    @Autowired
    UserInfoRepository userInfoRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo activeUser = userInfoRepository.findById(username).get();
        GrantedAuthority authority = new SimpleGrantedAuthority(activeUser.getRole());
        System.out.println(activeUser.getUserName());

        return (UserDetails)new User(activeUser.getUserName(),
                activeUser.getPassword(), Arrays.asList(authority));
    }
}
