package com.belajar.loginform.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class webview {

    @RequestMapping("/home")
    @ResponseBody
    public String index() {
        return "You made it!";
    }

    @PreAuthorize("hasRole('ROLE_SUPERVISORS')")
    @RequestMapping("/supervisor")
    @ResponseBody
    public String home() {
        return "ini supervisor";
    }

    @RequestMapping("/")
    public String login(){
        return "login.html";
    }

    @RequestMapping("/login-failed")
    public String loginError(Model model){
        model.addAttribute("loginError", true);
        return "login.html";
    }
}
